/*Таблица пользователей :*/

CREATE TABLE "public".users (
  id    int not null,
  name  varchar (64),
  constraint pkUsersID primary key (id)
);
/*Таблица протоколирования*/

CREATE TABLE "public".logs (
  text  varchar(256),
  data  timestamp without time zone
);



CREATE OR REPLACE FUNCTION "public".remove_user_from_company()
   RETURNS TRIGGER
AS $$
DECLARE
    emp_id int;
    v_user   varchar(64);
    v_retstr varchar(256);
BEGIN
    ELSIF TG_OP = 'DELETE' THEN
        emp_id = OLD.id;
        v_action := 'Remove employee ';
        v_retstr := v_action || v_user;
        UPDATE company SET employee_id=
        INSERT INTO "public".logs(text, data) values (v_retstr, NOW());
        RETURN OLD;
    END IF;
END;
$$ LANGUAGE plpgsql;

-- trigger insert & trigger update & trigger delete
CREATE TRIGGER trg_user
 AFTER INSERT OR UPDATE OR DELETE 
   ON "public".users FOR EACH ROW 
     EXECUTE PROCEDURE add_to_log ();