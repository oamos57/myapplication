

/*CREATE OR REPLACE FUNCTION delete_emp() RETURNS TRIGGER AS $$
DECLARE
 id int;
BEGIN
IF TG_OP = 'DELETE' THEN
        id=OLD.id;
        UPDATE company SET enloyees=deletebyvalue(enloyees, id);
        RETURN 1;
      end if;
END;
$$ LANGUAGE plpgsql;*/






CREATE OR REPLACE FUNCTION deletebyvalue(in_array INTEGER[], del_index INTEGER)
  RETURNS INTEGER[] AS
$BODY$declare
  res INTEGER[];
  i INTEGER := 0;
  k INTEGER := 0;
BEGIN
  FOR i IN array_lower(in_array, 1)..array_upper(in_array, 1)
  loop
    IF in_array[i] = del_index
    THEN
      k = k + 1;
    ELSE
      res[i - k] = in_array[i];
    END IF;
  END loop;
  RETURN res;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION delete_emp(id INTEGER) 
  RETURNS INTEGER AS
$BODY$
BEGIN
        DELETE FROM employee WHERE employee.id=id;
        UPDATE company SET enloyees=deletebyvalue(enloyees, id);
         RETURN id;
END;$BODY$
 LANGUAGE plpgsql;


















CREATE OR REPLACE FUNCTION dele_emp_del_arr() RETURNS TRIGGER AS $$
DECLARE
    old_id int;
BEGIN
IF TG_OP = 'DELETE' THEN
        old_id  = OLD.id;

        UPDATE companies SET employee_id=deletebyvalue(employee_id,old_id);
        RETURN OLD;
    END IF;
END;

$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION deletebyvalue(in_array INTEGER[], del_index INTEGER)
  RETURNS INTEGER[] AS
$BODY$declare
  res INTEGER[];
  i INTEGER := 0;
  k INTEGER := 0;
BEGIN
  FOR i IN array_lower(in_array, 1)..array_upper(in_array, 1)
  loop
    IF in_array[i] = del_index
    THEN
      k = k + 1;
    ELSE
      res[i - k] = in_array[i];
    END IF;
  END loop;
  RETURN res;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;