package com.example.myapplication.model;

import com.example.myapplication.annotation.Id;

public class BaseObject {

	@Id
	private int id;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
}
