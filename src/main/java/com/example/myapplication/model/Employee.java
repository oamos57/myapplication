package com.example.myapplication.model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;

import com.example.myapplication.annotation.Column;
import com.example.myapplication.annotation.Id;
import com.example.myapplication.annotation.OneToMany;
import com.example.myapplication.annotation.Table;

@SuppressWarnings("serial")
@Table(name="employees")
public class Employee extends BaseObject implements Serializable,Cloneable{

    @Column
	private String name;
	
	@Column
	private LocalDate birthday;
	
	@Column
	private String email;
	
	@Column
   @OneToMany(column="company.id")
	private String company_id;
	
	public Employee() {}
	

	public String getName() {
		return name;
	}
     public void setName(String name) {
    	 this.name=name;
     }
	public LocalDate getBirthday() {
		return birthday;
	}
	public String getEmail() {
		return email;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday=birthday;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id=company_id;
	}
	/*@Override
	public boolean equals(Object o) {
	if(o==null) return false;
		return this.email.equals(((Employee)o).getEmail());
	}
	@Override
	public int hashCode() {
		return this.email.hashCode();
	}*/
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name+","+birthday.toString()+","+email+","+company_id;
	}
}
