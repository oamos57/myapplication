package com.example.myapplication.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.example.myapplication.annotation.Column;
import com.example.myapplication.annotation.Id;
import com.example.myapplication.annotation.Table;;

@SuppressWarnings("serial")
@Table(name="company")
public class Company extends BaseObject implements Serializable,Cloneable{


	@Column
	private String name;
	
	@Column
	private String inn;
	
	
	@Column
	private String address;
	
    private List<Employee> eploees;
	
	public Company() {}
	

	public String getInn() {
		return inn;
	}

	public String getAddress() {
		return address;
	}


	public void setInn(String inn) {
		this.inn = inn;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Employee> getEmploees() {
		return eploees;
	}

	public void setEmploees(List<Employee> eploees) {
		this.eploees = eploees;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o) {
		if(Objects.isNull(o)) return false;
        if(!Objects.isNull(getName()))
	       if(this.getName().equals(((Company)o).getName()))
		       return true;
		return super.equals(o);
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	}
}
