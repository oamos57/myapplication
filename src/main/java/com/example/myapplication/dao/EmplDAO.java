package com.example.myapplication.dao;

import java.util.List;

import com.example.myapplication.model.Employee;

public interface EmplDAO {

	boolean updateEmployee(Employee employees);
	
	Employee get(int id);
	
	List<Employee> getEmployees();
	
	boolean addEmployee(Employee employee);
	
	boolean removeEmployee(int id);
	
	List<Employee> getEmployeeNotWork();

	boolean checkEmail(String email);
}
