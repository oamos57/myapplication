package com.example.myapplication.dao;

public class Factory {

	private static EmplDAO emplDAO;
	
	private static CompanyDAO companyDAO;
	
	private static Factory factory;
	
    private Factory(){}
	
	public static Factory getInstance() {
		if(factory==null)
			factory= new Factory();
		return factory;
	}
	
	
	public EmplDAO getEmployeeDAO() {
		if(emplDAO==null) emplDAO=  new EmplDaoImp();
		return emplDAO;
	}
	
	
	public CompanyDAO getCompanyDAO() {
		if(companyDAO==null) companyDAO=new CompanyDaoImp();
		return companyDAO;
	}
}
