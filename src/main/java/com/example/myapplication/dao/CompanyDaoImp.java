package com.example.myapplication.dao;

import static com.example.myapplication.database.Query.CREATE_COMPANY;
import static com.example.myapplication.database.Query.DELETE_COMPANY;
import static com.example.myapplication.database.Query.GET_ALL_COMPANY;
import static com.example.myapplication.database.Query.GET_COMPANY;
import static com.example.myapplication.database.Query.GET_EMPLOYEES_FROM_COMPANY;
import static com.example.myapplication.database.Query.UPDATE_COMPANY;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.example.myapplication.database.DBConnection;
import com.example.myapplication.model.Company;
import com.example.myapplication.model.Employee;


public class CompanyDaoImp implements CompanyDAO{

	private EmplDAO emplDAO=Factory.getInstance().getEmployeeDAO();
	
	 CompanyDaoImp() {}
	
	@Override
	public boolean update(Company companies) {
		
		Connection connection=DBConnection.getConnection();
    try( PreparedStatement ps=connection.prepareStatement(UPDATE_COMPANY)){
    	
    	 connection.setAutoCommit(false);
    	 ps.setString(1, companies.getInn());
    	 ps.setString(2, companies.getName());
    	 ps.setString(3, companies.getAddress());
    	 ps.setInt(4, companies.getId());
    	 ps.executeUpdate();
    	 
    	 List<Employee> employees=companies.getEmploees();
		    if(!employees.isEmpty())
		    for(Employee emp :employees) {
		    	emp.setCompany_id(companies.getName());
		    	emplDAO.updateEmployee(emp);
		    }
		    connection.commit();
    return true;
    } catch (SQLException e) {
    	try {
    		if(!Objects.isNull(connection))	connection.rollback();
		} catch (SQLException e1) {
             e1.printStackTrace();}
    }finally {
    	try {
    		if(!Objects.isNull(connection))	connection.setAutoCommit(true);
    		close(connection);
		} catch (SQLException e1) {
             e1.printStackTrace();}
	}
	return false;
		
	}

	@Override
	public Company get(int id) throws SQLException {
		ResultSet rs=null;		 
	    try(Connection connection=DBConnection.getConnection();
	    		PreparedStatement ps=connection.prepareStatement(GET_COMPANY)){
	    	
	    	ps.setInt(1, id);
	    	
	        rs=ps.executeQuery();
	       
	     Company    com=new Company();
	    	while(rs.next()) {
	    	    com.setId(rs.getInt(1));
	    	    com.setInn(rs.getString(2));
	    		com.setName(rs.getString(3));
	    		com.setAddress(rs.getString(4));
	    		com.setEmploees(getEmployeesCompany(com.getName()));
	    				
	    	}
	    	return com;
	    }catch (SQLException e) {			
		}finally {
			close(rs);
			
		}	    
		return null;
	}

	@Override
	public void remove(Company id) {		
		  Connection conn=DBConnection.getConnection();
				 try( PreparedStatement ps=conn.prepareStatement(DELETE_COMPANY)){
			  ps.setInt(1, id.getId());
			  conn.setAutoCommit(false);
			  
			  ps.execute();
			    List<Employee> employees=getEmployeesCompany(id.getName());
			    if(!employees.isEmpty())
			    for(Employee emp :employees) {
			    	emp.setCompany_id("null");
			    	emplDAO.updateEmployee(emp);
			    }
			  conn.commit();
			
		  } catch (SQLException e) {
			  try {
				if(!Objects.isNull(conn)) conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		  }	finally {
			    try {
			    	if(!Objects.isNull(conn)) conn.setAutoCommit(true);
			    	close(conn);
				} catch (SQLException e) {
					e.printStackTrace();
				}
		  }	
	}


	@Override
	public List<Company> allCompanies() {
		List<Company> companies= new ArrayList<>();
		ResultSet rs=null;
		  try(Connection connection=DBConnection.getConnection();
		    		PreparedStatement ps=connection.prepareStatement(GET_ALL_COMPANY)){
			    rs=ps.executeQuery();
			     Company    com;
			    	while(rs.next()) {
			    		com=new Company();
			    	    com.setId(rs.getInt(1));
			    	    com.setInn(rs.getString(2));
			    		com.setName(rs.getString(3));
			    		com.setAddress(rs.getString(4));
			    		com.setEmploees(getEmployeesCompany(com.getName()));
			    		companies.add(com);
			    	}
			    	return companies;
		  } catch (SQLException e) {			
			e.printStackTrace();
		}finally {
			close(rs);
		}
		return null;
	}

	@Override
	public List<Employee> getEmployeesCompany(String name) {
		List<Employee> employees= new ArrayList<>();
		
		ResultSet rs=null;
		  try(Connection connection=DBConnection.getConnection();
		    		PreparedStatement ps=connection.prepareStatement(GET_EMPLOYEES_FROM_COMPANY)){
			  ps.setString(1, name);
			    rs=ps.executeQuery();
			 Employee eml;
			 while(rs.next()) {
				 eml= new Employee();
				 eml.setId(rs.getInt(1));
				eml.setName(rs.getString(2));
				eml.setBirthday(rs.getDate(3).toLocalDate());
				eml.setEmail(rs.getString(4));
				employees.add(eml);
			 }
			    return employees;
			    
		  } catch (SQLException e) {
			e.printStackTrace();
		}finally {close(rs);}
		  
		return null;
	}

	private void close(AutoCloseable...closeables) {
		for(AutoCloseable au : closeables) {
			if(au==null) continue;
			try {
				au.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	@Override
	public boolean addCompany(Company company) {
		Connection connection=DBConnection.getConnection();
		
		  try(PreparedStatement ps=connection.prepareStatement(CREATE_COMPANY)){
			  connection.setAutoCommit(false);  
		    	 ps.setString(1, company.getInn());
		    	 ps.setString(2, company.getName());
		    	 ps.setString(3, company.getAddress());
		         ps.executeUpdate();		         
		         List<Employee> employees=company.getEmploees();

		         
		            for(Employee employee:employees)
		            {
		            	employee.setCompany_id(company.getName());
		            	emplDAO.updateEmployee(employee);
		            }
		            connection.commit();
		         
		    	 return true;
		  } catch (SQLException e) {
			  try {
				  if(connection!=null)
				       connection.rollback();
			} catch (SQLException e1) {}
			  e.printStackTrace();
		}finally {
			try {
				 if(Objects.isNull(connection)) {
				    connection.setAutoCommit(true);
					connection.close();
				 }
			} catch (SQLException e) {e.printStackTrace();}
		}
		return false;
	}

	@Override
	public boolean checkData(String value,String query) {
		ResultSet rs=null;		 
	    try(Connection connection=DBConnection.getConnection();
	    		PreparedStatement ps=connection.prepareStatement(query)){
	    	
	    	ps.setString(1, value);
	    	
	        rs=ps.executeQuery();

	        String temp=null;
	    	while(rs.next()) 
	    		temp=rs.getString(3);
		
	    	
	    	return value.equals(temp);
	    }catch (SQLException e) {			
		}finally {
			close(rs);
		}	    
		return true;
	}

	


}
