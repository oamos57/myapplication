package com.example.myapplication.dao;


import static com.example.myapplication.database.Query.CREATE_EMPLOYEE;
import static com.example.myapplication.database.Query.DELETE_EMPLOYEE;
import static com.example.myapplication.database.Query.GET_ALL_EMPLOYEES;
import static com.example.myapplication.database.Query.GET_EMPLOYEE;
import static com.example.myapplication.database.Query.GET_EMPLOYEES_BY_WORK;
import static com.example.myapplication.database.Query.UPDATE_EMPLOYEE;
import static com.example.myapplication.database.Query.CHECK_EMAIL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.example.myapplication.database.DBConnection;
import com.example.myapplication.model.Employee;

public class EmplDaoImp implements EmplDAO{

	 EmplDaoImp() {}
	
	@Override
	public boolean updateEmployee(Employee employees) {
	    try(Connection connection=DBConnection.getConnection();
	    	     PreparedStatement ps=connection.prepareStatement(UPDATE_EMPLOYEE)){
	    
	    	    	 ps.setString(1, employees.getName());
	    	    	 ps.setDate(2, Date.valueOf(employees.getBirthday()));
	    	    	 ps.setString(3, employees.getEmail());
	    	    	 ps.setString(4, employees.getCompany_id());
	    	    	 ps.setInt(5, employees.getId());
	    	    	 ps.executeUpdate();
	    	    return true;
	    	    } catch (SQLException e) {
	    	    	e.printStackTrace();			
	    	    }
	    		return false;
		
	}

	@Override
	public Employee get(int id) {
		ResultSet rs=null;		 
	    try(Connection connection=DBConnection.getConnection();
	    		PreparedStatement ps=connection.prepareStatement(GET_EMPLOYEE)){
	    	
	    	ps.setInt(1, id);
	        rs=ps.executeQuery();      
	     Employee em=new Employee();
	    	while(rs.next()) {
	    		em.setId(rs.getInt(1));
	    		em.setName(rs.getString(2));
	    		em.setBirthday(rs.getDate(3).toLocalDate());
	    		em.setEmail(rs.getString(4));
	    		em.setCompany_id(rs.getString(5));
	    				
	    	}
	    	return em;
	    }catch (SQLException e) {			
		}finally {
			close(rs);
		}	    
		return null;
	}



	@Override
	public List<Employee> getEmployees() {
		List<Employee> employees= new ArrayList<>();
		ResultSet rs=null;
		  try(Connection connection=DBConnection.getConnection();
		    		PreparedStatement ps=connection.prepareStatement(GET_ALL_EMPLOYEES)){
			    rs=ps.executeQuery();
			     Employee   emp;
			    	while(rs.next()) {
			    		emp=new Employee();
			    		emp.setId(rs.getInt(1));
			    		emp.setName(rs.getString(2));
			    		emp.setBirthday(rs.getDate(3).toLocalDate());
			    		emp.setEmail(rs.getString(4));
			    		emp.setCompany_id(rs.getString(5));
			    		employees.add(emp);
			    	}
			    	return employees;
		  } catch (SQLException e) {			
			e.printStackTrace();
		}finally {
			close(rs);
		}
		return null;
	}

	@Override
	public boolean addEmployee(Employee employee) {
		 try(Connection connection=DBConnection.getConnection();
			       PreparedStatement ps=connection.prepareStatement(CREATE_EMPLOYEE)){
			    	 ps.setString(1, employee.getName());
			    	 ps.setDate(2, Date.valueOf(employee.getBirthday()));
			    	 ps.setString(3, employee.getEmail());    	
			    	 ps.setString(4, employee.getCompany_id());
			         ps.execute();
			    	 return true;
			  } catch (SQLException e) {
				  e.printStackTrace();
			}
			return false;
	}

	@Override
	public boolean removeEmployee(int id) {
		  try(Connection conn=DBConnection.getConnection();
				  PreparedStatement ps=conn.prepareStatement(DELETE_EMPLOYEE)){
			  ps.setInt(1, id);
			  ps.execute();
			  return true;
		  } catch (SQLException e) {
			e.printStackTrace();
		  }
		return true;	
	}
	
	private void close(AutoCloseable...closeables) {
		for(AutoCloseable au : closeables) {
			if(au==null) continue;
			try {
				au.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Employee> getEmployeeNotWork() {
		List<Employee> employees= new ArrayList<>();
		ResultSet rs=null;
		  try(Connection connection=DBConnection.getConnection();
		    		PreparedStatement ps=connection.prepareStatement(GET_EMPLOYEES_BY_WORK)){
			    ps.setString(1, "null");

			    rs=ps.executeQuery();
			     Employee   emp;
			    	while(rs.next()) {
			    		emp=new Employee();
			    		emp.setId(rs.getInt(1));
			    		emp.setName(rs.getString(2));
			    		emp.setBirthday(rs.getDate(3).toLocalDate());
			    		emp.setEmail(rs.getString(4));
			    		emp.setCompany_id(rs.getString(5));
			    		employees.add(emp);
			    	}
			    	return employees;
		  } catch (SQLException e) {			
			e.printStackTrace();
		}finally {
			close(rs);
		}
		return null;
	}

	@Override
	public boolean checkEmail(String email) {
		ResultSet rs=null;
		  try(Connection connection=DBConnection.getConnection();
		    		PreparedStatement ps=connection.prepareStatement(CHECK_EMAIL)){
			    ps.setString(1, email);

			    rs=ps.executeQuery();
                 String tempEmail=null;
			    	while(rs.next()) {

			    		tempEmail=rs.getString(1);

			    	}
			    	return email.equals(tempEmail);
		  } catch (SQLException e) {			
			e.printStackTrace();
		}finally {
			close(rs);
		}

		return true;
	}
}
