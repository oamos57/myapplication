package com.example.myapplication.dao;

import java.sql.SQLException;
import java.util.List;

import com.example.myapplication.model.Company;
import com.example.myapplication.model.Employee;

public interface CompanyDAO {
	
	boolean update(Company companies);

	Company get(int id) throws SQLException;
	
	void remove(Company id);
	
	boolean addCompany(Company company);
	
	List<Company> allCompanies();
	
	List<Employee> getEmployeesCompany(String name);

	boolean checkData(String value,String query);
	
  
	
}
