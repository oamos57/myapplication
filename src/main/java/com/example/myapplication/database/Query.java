package com.example.myapplication.database;

public class Query {

	public static final String CREATE_EMPLOYEE="INSERT INTO employees(name,birthday,email,company_id) "
			                                 + "VALUES(?,?,?,?)";
	
	public static final String UPDATE_EMPLOYEE="UPDATE employees SET name=?,birthday=?,email=? ,company_id=?"
			                                 + "WHERE id=?";
	
	public static final String DELETE_EMPLOYEE="DELETE FROM employees WHERE id=?"; 
	
	public static final String GET_ALL_EMPLOYEES="SELECT * FROM employees";

	
	public static final String GET_EMPLOYEES_FROM_COMPANY=
			"SELECT * FROM employees WHERE company_id=?";
	
	public static final String CHECK_EMAIL= "SELECT email FROM employees WHERE email=?";
	
	public static final String GET_EMPLOYEES_BY_WORK="SELECT * FROM employees WHERE  company_id=?";
	
	//company_id
	public static final String GET_EMPLOYEE="SELECT * FROM employees WHERE id=?";
	
	
	public static final String CREATE_COMPANY="INSERT INTO companies(inn,name,address)" + 
			" VALUES(?,?,?);";
	
	public static final String UPDATE_COMPANY="UPDATE companies SET inn=?,name=?,address=? "
		                            	+      "WHERE id=?";
	
	public static final String GET_COMPANY="SELECT * FROM companies WHERE id=?"; 
	
	public static final String GET_ALL_COMPANY="SELECT * FROM companies"; 
	
	public static final String DELETE_COMPANY="DELETE FROM companies WHERE id=?";
	
	public static final String CHECK_NAME    ="SELECT name FROM companies WHERE name=?";
	
	public static final String CHECK_INN    ="SELECT inn FROM companies WHERE inn=?";

}
