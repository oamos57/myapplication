package com.example.myapplication.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

import org.postgresql.ds.PGPoolingDataSource;

public class DBConnection {

	private static PGPoolingDataSource pds;
	
	private DBConnection(){}
	
	//private static Connection connection;
	
	
	static {

		pds= new PGPoolingDataSource();
		pds.setServerName("localhost");
		pds.setDatabaseName("city");
		pds.setUser("postgres");
		pds.setPassword("postgres");
		pds.setPortNumber(5432);
		pds.setDataSourceName("My connection");
		pds.setInitialConnections(3);
		pds.setMaxConnections(10);
/*		try {
	//	 connection=pds.getConnection();
		} catch (SQLException e) {
		   System.out.println(e.getMessage());
		}*/
	}
     
	public static Connection getConnection() {

		try {
			return pds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
/*	
	public static boolean isConnection() {
		return !Objects.isNull(connection);
	}*/
}