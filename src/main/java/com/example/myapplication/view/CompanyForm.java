package com.example.myapplication.view;

import java.util.ArrayList;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;

import com.example.myapplication.dao.CompanyDAO;
import com.example.myapplication.dao.EmplDAO;
import com.example.myapplication.dao.Factory;
import com.example.myapplication.database.Query;
import com.example.myapplication.model.Company;
import com.example.myapplication.model.Employee;
import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;





@SuppressWarnings("serial")
public class CompanyForm extends FormLayout{
	
	    private Factory f=Factory.getInstance();
	    private EmplDAO serviceEmpl= f.getEmployeeDAO();
	    private CompanyDAO serviceCompany=f.getCompanyDAO();
	    
	    ComboBox<Employee> employees=new ComboBox<>();
	    
	    private TextField name = new TextField("Имя");
	    private TextField inn = new TextField("Inn");
	    private TextField address = new TextField("Адрес");

	    private Panel panel= new Panel("Сотрудники");
	    private VerticalLayout forPanel= new VerticalLayout();
	    
	    private Button save = new Button("Save");
	    private Button delete = new Button("Delete");
	    
	    private Binder<Company> binder = new Binder<>(Company.class);
	    
	    private Company company;
	    private CompView verticalLayout;
	    
    	private List<Employee> empList;
	    
	    private boolean created;
	    
	    
	    private String oldName;
	    
	    
	    private String oldInn;
	    
	    public CompanyForm(CompView verticalLayout) {  
	    	this.verticalLayout=verticalLayout;
	    	//validBinder();
	    	name.setRequiredIndicatorVisible(true);
	    	inn.setRequiredIndicatorVisible(true);
	    	address.setRequiredIndicatorVisible(true);
	    	
	    	 setSizeUndefined();
	         HorizontalLayout buttons = new HorizontalLayout(save, delete);
	         addComponents(name, inn, address, employees,getPanelEmpl(), buttons);
             
	         employees.setItems(serviceEmpl.getEmployeeNotWork());         
	         employees.setItemCaptionGenerator(Employee::getName);
	         employees.setEmptySelectionAllowed(false);
		     comboListener();
		     
		     
	         save.setStyleName(ValoTheme.BUTTON_PRIMARY);
	         save.setClickShortcut(KeyCode.ENTER);

	         binder.bindInstanceFields(this);
         
           
             
	         save.addClickListener(e -> this.save());
	         delete.addClickListener(e -> this.delete());
	         
		}
	    
/*	    public void validBinder() {
	    	binder.forField(this.name).withNullRepresentation("").
	    	withValidator(str -> str.length() >2,"Не меньше двух симовлов").
	    	asRequired("поле не может быть пустым").bind(Company::getName,Company::setName);
	    	
	    }*/
	    
	    private Panel getPanelEmpl() {
	   	 panel.setWidth("183");
		 panel.setHeight("200");
		 panel.setContent(forPanel);
		 return panel;
		 
	    }
	    
	    private void validForm() {
	    	
	        address.addValueChangeListener((e)->{
				 if(address.getValue().length()<6) {
					 address.setComponentError(new UserError("Инн должно содержать не менее 2 символов"));
					}
				 if(address.getValue().isEmpty()) {
					 address.setComponentError(new UserError("empty field"));
					}
			 });
	    	
	    	if(created) {
	    	name.addValueChangeListener((e)->{
				if(name.getValue().length()<2) {
					name.setComponentError(new UserError("Имя должно содержать не менее 2 символов"));
					}
				if(serviceCompany.checkData(name.getValue(),Query.CHECK_NAME)) {
					name.setComponentError(new UserError("name is  used"));
					
				}
				if(name.getValue().isEmpty()) {
					name.setComponentError(new UserError("empty field"));
				}
				});
			
				
			 inn.addValueChangeListener((e)->{
				if(inn.getValue().length()<6) {
					inn.setComponentError(new UserError("Инн должно содержать не менее 2 символов"));
				}
				if(serviceCompany.checkData(inn.getValue(),Query.CHECK_INN)) {
					inn.setComponentError(new UserError("email address is used"));
				}
				if(inn.getValue().isEmpty()) {
					inn.setComponentError(new UserError("empty field"));
				}
			});
		
			}else{
				
			 oldName=new String(company.getName());
			 oldInn= new String(company.getInn());
				name.addValueChangeListener((e)->{
					
					if(name.getValue().length()<4) {
						name.setComponentError(new UserError("Имя должно содержать не менее 2 символов"));
					}		
		
					if(serviceCompany.checkData(name.getValue(),Query.CHECK_NAME) && 
							!oldName.equals(name.getValue())) {
						     name.setComponentError(new UserError("name address is used"));
					}
					
				});
                inn.addValueChangeListener((e)->{
					
					if(inn.getValue().length()<4) {
						inn.setComponentError(new UserError("Имя должно содержать не менее двух символов"));
					}		
		
					if(serviceCompany.checkData(inn.getValue(),Query.CHECK_INN) && 
							!oldInn.equals(inn.getValue())) {
						     inn.setComponentError(new UserError("name address is used"));
					}
					if(inn.getValue().isEmpty()) {
						inn.setComponentError(new UserError("empty field"));
					}
				});
			}
		}	
	    
	    
	    
	    public void employeesInCompany() {
	  
	    		forPanel.removeAllComponents();
	    	List<Employee> empList=company.getEmploees();
	    	if(!empList.isEmpty()) 
	    		for(Employee em: empList) {
	    			AddEmpl empl= new AddEmpl(em);
	    			 forPanel.addComponent(empl);  			 
	    		}
                    
	    	}
	    
	    public void clearPanel() {
	    	forPanel.removeAllComponents();
	    	empList.clear();
	    }
	    
	    private void comboListener() {
	    	employees.addValueChangeListener((e)->{
	    		   
		    	 AddEmpl empl= new AddEmpl(e.getValue());
		    	 if(forPanel.getComponentIndex(empl)==-1)
		    		 forPanel.addComponent(empl);
	               empList.add(empl.getEmployee()); 
		    	   
		       });
	    }
	    
	    public void updateCombo() {
	    	List<Employee> employeeList=serviceEmpl.getEmployeeNotWork();
	    	employees.setItems(employeeList);
	    	//employees.setValue(null);
	    }
	    
	    public void setCompany(Company company) {
	        this.company = company;
	        binder.setBean(company);

	        empList= new ArrayList<>();
	        // Show delete button for only customers already in the database
	        delete.setVisible(true);
	        setVisible(true);
	        name.selectAll();
	        validForm();
	    }
	    private void delete() {
	    	serviceCompany.remove(company);
	        verticalLayout.updateList();
	        setVisible(false);
	    }

	    private void save() {
	    	
	    	if(isError(name,"empty field")) return;
	    	if(isError(inn,"empty field")) return;
	    	if(isError(address,"empty field")) return;
	    	
	    	company.setEmploees(empList);
	    	if(created) {
	    	
	    	serviceCompany.addCompany(company);
	    	created=false;
	    	}else {
	    		serviceCompany.update(company);
	    	}
	        verticalLayout.updateList();
	        setVisible(false);
	    }
	    
	    private boolean isError(TextField component,String messageError) {
	    	if(component.getValue().isEmpty()) {
	    		component.setComponentError(new UserError(messageError));
	    		return true;
			}
	    	return component.getComponentError()!=null;
	    		
	    }
	    
	    
	    public boolean isCreated() {
			return created;
		}



		public void setCreated(boolean created) {
			this.created = created;
		}




		class AddEmpl extends HorizontalLayout{
			
			private Employee employee;
			private Button close;
			private Label name;
			
			public AddEmpl(Employee employee) {
				this.employee=employee;
				setSizeUndefined();
				name= new Label(employee.getName());
	
			
				   UI.getCurrent().getPage().getStyles().add(".names{"
					   		+ "font-size:12px}");
				name.setStyleName("names");
				close= new Button("×");
				close.setWidth("15px");
				close.setHeight("15px");
				close.setSizeUndefined();
			
				 UI.getCurrent().getPage().getStyles().add(".del{"
				 		+ "position:relative"
				 		+ ""
				 		+ "}");
				   UI.getCurrent().getPage().getStyles().add(".del.v-button-del{"
						   + "  width: 11px;\n" + 
					   		"    height: 11px;\n"
					   		+ "color:red;\n" + 
					   		"position: relative;"+
					   		//"    border: 0 none transparent;\n" + 
					   		"    padding:0;\n" + 
					   		"    margin:1;}");
	                close.addStyleName("del");
		

				close.addClickListener((e)->{
					if(empList.contains(this.getEmployee())) {			
					forPanel.removeComponent(this);
					empList.remove(this.getEmployee());
					}else {
						
						  ConfirmDialog.show(getUI(), "Please Confirm:", "Подтвердите действие?",
				    		        "Да", "Отмена", new ConfirmDialog.Listener() {

				    		            public void onClose(ConfirmDialog dialog) {
				    		            	
				    		                if (dialog.isConfirmed()) {
				    		                	
				    		                employee.setCompany_id("null");
				    	                    serviceEmpl.updateEmployee(employee);
				    	                    forPanel.removeComponent(new AddEmpl(employee));
				    	                    company.getEmploees().remove(employee);
				    		                } 
				    		            }
				    		        });
						 
				    	                    
					}
					});
				addComponents(name,close);
				
			}

			public Employee getEmployee() {
				return employee;
			}

	        @Override
	        	public boolean equals(Object obj) {
	        	if(obj==null) return false;
	        		return employee.getEmail().equals(((AddEmpl) obj).getEmployee().getEmail());
	        	}
			@Override
			public int hashCode() {
				return employee.getId();
			}
		}
}
