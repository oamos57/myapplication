package com.example.myapplication.view;

import java.util.Objects;

import com.example.myapplication.dao.CompanyDAO;
import com.example.myapplication.dao.EmplDAO;
import com.example.myapplication.dao.Factory;
import com.example.myapplication.model.Company;
import com.example.myapplication.model.Employee;
import com.flowingcode.vaadin.addons.errorwindow.WindowErrorHandler;
import com.vaadin.data.Binder;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.UserError;
import com.vaadin.shared.ui.ErrorLevel;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class EmployeeForm extends FormLayout{

	
	
	private CompanyDAO compService=Factory.getInstance().getCompanyDAO();
	private EmplDAO empService=Factory.getInstance().getEmployeeDAO();
	private TextField name= new TextField("Имя");
	private DateField birthday = new DateField("Birthday");
	private TextField email = new TextField("Контакты");
	private ComboBox<Company> company= new ComboBox<Company>("ККомпания");
	
	private EmployeeView verticalLayout;
	private Binder<Employee> binder= new Binder<>(Employee.class);
	
	 private Button save = new Button("Save");
	 private Button delete = new Button("Delete");
	 
	 /**
	  * 
	  * основной объект 
	  */
	 private Employee employee;
	 
	 /**
	  * 
	  * метка отвечающая за создание или обновление объекта
	  * true - создать
	  * false - обновить
	  */
	 private boolean created;
	    
	 
	 /**
	  * 
	  * хранит данные о старом email при обновлении
	  * 
	  * в случае, если пользователь передумает изменять email, его
	  * старый email останется валидным
	  * 
	  */
	 private String oldEmail;
	 
	 
	 
	public EmployeeForm(EmployeeView verticalLayout) {
         setSizeUndefined();
	     this.verticalLayout=verticalLayout;
 
         HorizontalLayout buttons = new HorizontalLayout(save, delete);
         addComponents(name, birthday,email,company, buttons);
		company.setItems(compService.allCompanies());         
		company.setItemCaptionGenerator(Company::getName);
		company.setEmptySelectionAllowed(false);
         save.setStyleName(ValoTheme.BUTTON_PRIMARY);
         save.setClickShortcut(KeyCode.ENTER);
     	
		binder.bindInstanceFields(this);
		
         name.addValueChangeListener((e)->{
			if(name.getValue().length()<2) {
				name.setComponentError(new UserError("Имя должно содержать не менее 2 символов"));
	   
			}
			});
		
        save.addClickListener(e -> this.save());
        delete.addClickListener(e -> this.delete());
        
      
	}
	
	   
	   public void getComboValue() {
		  Company comp= new Company();
		  comp.setName(employee.getCompany_id());
		   company.setValue(comp);
		  
	   }
	 
	   public void cleanCombo() {
		company.setValue(null);   
	   }
	   
		public void setEmployee(Employee value) {
	      this.employee=value;
          binder.setBean(value);


       delete.setVisible(true);
       setVisible(true);
       name.selectAll();
      
       validData();
		 
	}
		
	private void validData() {
		name.addValueChangeListener((e)->{
			if(name.getValue().length()<2) {
				name.setComponentError(new UserError("Имя должно содержать не менее 2 символов"));
	   
			}
			});
		if(created) {
			
		email.addValueChangeListener((e)->{
			if(email.getValue().length()<6) {
				email.setComponentError(new UserError("Имя должно содержать не менее 2 символов"));
			}
			if(empService.checkEmail(email.getValue())) {
				email.setComponentError(new UserError("email address is used"));
			}
		});
		}else{
		 oldEmail=new String(employee.getEmail());
			email.addValueChangeListener((e)->{
				
				if(email.getValue().length()<6) {
					email.setComponentError(new UserError("Имя должно содержать не менее 2 символов"));
				}		
	
				if(empService.checkEmail(email.getValue()) && 
						!oldEmail.equals(email.getValue())) {
					     email.setComponentError(new UserError("email address is used"));
				}
			
			});
		}
	}	
		
	private void delete() {
        verticalLayout.getGrid().remove(employee,
        		()->empService.removeEmployee(employee.getId()));		
        setVisible(false);
	}


	private void save() {	
	  if( isError(name, "Это поле дожно быть заполненно")) return;
	  
	  if(Objects.isNull(birthday.getValue())) {
		  birthday.setComponentError(new UserError("Заполните дату"));
		  return;
	  }
	  
	  if( isError(email, "Это поле дожно быть заполненно")) return;


	         /*       employee.setName(name.getValue());
				    employee.setEmail(email.getValue());
				    employee.setBirthday(birthday.getValue());*/
	          if(company.getValue()!=null)
			        employee.setCompany_id(company.getValue().getName());
				else
				    employee.setCompany_id("null");
		      if(created) {
				verticalLayout.getGrid().add(employee,
							()->empService.addEmployee(employee));
			     	created=false;
	          }else {
		          verticalLayout.getGrid().update
		           (employee,()->empService.updateEmployee(employee));	
		          }
        setVisible(false);
	}
     
    private boolean isError(TextField component,String errorMessage ) {
    	if(component.getValue().isEmpty()) {
    		component.setComponentError(new UserError("empty field"));
    		return true;
		}
    	return component.getComponentError()!=null;
    		
    }

	public boolean isCreated() {
		return created;
	}


	public void setCreated(boolean created) {
		this.created = created;
	}




}
