package com.example.myapplication.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.myapplication.dao.CompanyDAO;
import com.example.myapplication.model.Company;



public class FilterCompany {


	public  List<Company> findAll(String stringFilter,CompanyDAO session) {
			
		List<Company> l=session.allCompanies();

		ArrayList<Company> viewList=new ArrayList<Company>();
		for (Company contact : l) {

				boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
						|| contact.getName().toLowerCase().contains(stringFilter.toLowerCase());
				if (passesFilter) {
					viewList.add(contact);
				
				}
		}
		Collections.sort(viewList, new Comparator<Company>() {
          
			@Override
			public int compare(Company o1, Company o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		return viewList;
	}
	
}
