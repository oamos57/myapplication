package com.example.myapplication.view;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
       
    	Label title= new Label("Menu");
    	title.addStyleName(ValoTheme.MENU_TITLE);
    	
    	Button item1= new Button("Компании",e->getNavigator().navigateTo("companyView"));
    	item1.addStyleNames(ValoTheme.BUTTON_LINK,ValoTheme.MENU_ITEM);
    	
    	Button item2= new Button("Paботники",e->getNavigator().navigateTo("employeeView"));
    	item2.addStyleNames(ValoTheme.BUTTON_LINK,ValoTheme.MENU_ITEM);
    	
    	Button item3= new Button("На главную",e->getNavigator().navigateTo(""));
    	item3.addStyleNames(ValoTheme.BUTTON_LINK,ValoTheme.MENU_ITEM);
    	
    	CssLayout menu= new CssLayout(title,item1,item2,item3);
    	menu.addStyleName(ValoTheme.MENU_ROOT);
    	menu.setHeight(100, Unit.PERCENTAGE);
    	CssLayout viewContainer= new CssLayout();
    	
    	viewContainer.setSizeFull();
    	
    	
    	HorizontalLayout gHorizontalLayout= new HorizontalLayout(menu,viewContainer);
    	gHorizontalLayout.setExpandRatio(viewContainer, 1);
    	gHorizontalLayout.setSizeFull();
    	setContent(gHorizontalLayout);
    	
    	setSizeFull();
    	
    	Navigator navigator = new Navigator(this, viewContainer);
    	navigator.addView("", MainView.class);
    	navigator.addView("companyView", CompView.class);
    	navigator.addView("employeeView", EmployeeView.class);
    	navigator.addView("error", ErrorPage.class);

    }

  
    
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
