package com.example.myapplication.view;



import com.vaadin.navigator.View;
import com.vaadin.ui.Composite;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;

public class MainView extends Composite implements View{
	
	public MainView() {
		
	   Label l=new Label("Добро пожаловать на главную страницу");
	  
	   UI.getCurrent().getPage().getStyles().add(""
	   		+ ".title{"
	   		+ "color: red;"
	   		+ "font-size:30px;"
	   		+ "}");
	    l.setStyleName("title");
	   setCompositionRoot(l);
	}

}
