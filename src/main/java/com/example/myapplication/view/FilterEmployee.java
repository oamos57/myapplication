package com.example.myapplication.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.myapplication.dao.EmplDAO;
import com.example.myapplication.model.Employee;

public class FilterEmployee {

	public  List<Employee> findAll(String stringFilter,EmplDAO session) {
		
		List<Employee> l=session.getEmployees();

		ArrayList<Employee> viewList=new ArrayList<Employee>();
		for (Employee contact : l) {

				boolean passesFilter = (stringFilter == null || stringFilter.isEmpty())
						|| contact.getName().toLowerCase().contains(stringFilter.toLowerCase());
				if (passesFilter) {
					viewList.add(contact);
				
				}
		}
		Collections.sort(viewList, new Comparator<Employee>() {
          
			@Override
			public int compare(Employee o1, Employee o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		return viewList;
	}
	
}
