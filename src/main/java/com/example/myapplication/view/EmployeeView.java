package com.example.myapplication.view;

import java.util.List;

import com.example.myapplication.dao.EmplDAO;
import com.example.myapplication.dao.Factory;
import com.example.myapplication.model.Employee;
import com.example.myapplication.ui.MyGrid;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class EmployeeView extends VerticalLayout implements View{

	
	    private MyGrid<Employee> grid = new MyGrid<>(Employee.class);
	    private TextField filterText = new TextField();
		private  Factory f=Factory.getInstance();
		private EmplDAO service=f.getEmployeeDAO();
		private EmployeeForm form= new EmployeeForm(this);
		private final FilterEmployee fe= new FilterEmployee();

		
	public EmployeeView() {

		setWidth(100,Unit.PERCENTAGE);
		filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        
        @SuppressWarnings("deprecation")
		Button clearFilterTextBtn = new Button(FontAwesome.TIMES);
        clearFilterTextBtn.setDescription("Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());
        
        CssLayout filtering = new CssLayout();
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        filtering.addComponents(filterText, clearFilterTextBtn);

        
        Button addCustomerBtn = new Button("Add new Employee");
        addCustomerBtn.addClickListener(e -> {
            grid.asSingleSelect().clear();
            form.setCreated(true);
          form.setEmployee(new Employee());
          form.cleanCombo();
          
        });
        Button downloadExcel = new   Button("Download to excel");
        
        
        Button test= new   Button("test");
        
       test.addClickListener((e)->{
    	   
    	  
       });
        
        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn,downloadExcel,test);

        grid.setColumns("name", "birthday", "email","company_id");
       
         HorizontalLayout main = new HorizontalLayout(grid, form);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);
          addComponents(toolbar, main);
    

        // fetch list of Customers from service and assign it to Grid
        updateList();

        
      form.setVisible(false);
       
       grid.asSingleSelect().addValueChangeListener(event -> {

           if (event.getValue() == null) {
              form.setVisible(false);
           } else {
              form.setEmployee(event.getValue());
              form.getComboValue();
           }
           form.setCreated(false);
       });
      creatExcel(downloadExcel);
  
	}
    public MyGrid<Employee> getGrid(){
    	return grid;
      }
	
	   public void updateList() {
	        List<Employee> customers = fe.findAll(filterText.getValue(), service);
	        grid.setItems(customers);
	    }
	   

	   
	   private void creatExcel(Button b) {
		   ExcelExport ee=new ExcelExport("src/main/resources/employees.xls");
		   ee.creatSheet("employees");
		   ee.setMainTitle("Employees");
		   ee.setColumnName("Имя","Д-рождения","E-mail","М. работы");
		   ee.createExcel(grid);
		   ee.download(b);
		   
	   }
	
}
