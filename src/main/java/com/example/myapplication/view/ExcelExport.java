package com.example.myapplication.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.vaadin.data.provider.Query;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;

public class ExcelExport{

	private String path;
	private String name;
	private File   file;
	 Map<String, CellStyle> styles;
	private int row    = 0; 
    
	private CellStyle headerCellStyle;
	
	private XSSFWorkbook workbook;
	private Sheet        sheet;
	private Row          r;
	public ExcelExport(String path) {
		this.path=path;
	
		file= new File(path);
		name= file.getName();
		workbook = new XSSFWorkbook();
		styles = createStyles(workbook);
	}
	
	
	public void creatSheet(String name) {
		sheet=workbook.createSheet(name);
	}
	
	
	public void setColumnName(String...columnName) {
		r=sheet.createRow(row);
		r.setHeightInPoints(40);
        Cell headerCell;
		for(int i=0;i<columnName.length;i++) {
		/*	r.createCell(i).setCellValue(columnName[i]);
			r.setRowStyle(headerCellStyle);
			r.setRowStyle(styles.get("header"));*/
			   headerCell = r.createCell(i);
	            headerCell.setCellValue(columnName[i]);
	            headerCell.setCellStyle(styles.get("header"));
		}
		row++;
	}
	
	

	public void setMainTitle(String title) {
		row=0;
	     Row titleRow = sheet.createRow(0);
	        titleRow.setHeightInPoints(45);
	        Cell titleCell = titleRow.createCell(0);
	        titleCell.setCellValue(title);
	        titleCell.setCellStyle(styles.get("title"));
	        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));
	        row++;
	}
	@SuppressWarnings("hiding")
	public <T>  void createExcel(Grid<T> grid) {

		
		List<T> list = 
	      		(List<T>) grid.getDataProvider().
	      		fetch(new Query<>()).collect(Collectors.toList());
		
		
		for(int i=0;i<list.size();i++,row++) {
			
			Object o=list.get(i);
			Class<T> clas=(Class) o.getClass();
			Field[] field=clas.getDeclaredFields();
		    r=sheet.createRow(row);
		 
		    try {

				for(int count=0; count<field.length;count++) {
					field[count].setAccessible(true);
					r.createCell(count).setCellValue(field[count].get(o).toString());
					field[count].setAccessible(false);
				}	
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
	      //finally set column widths, the width is measured in units of 1/256th of a character width
        sheet.setColumnWidth(0, 10*256); //30 characters wide
        for (int i = 1; i < 4; i++) {
            sheet.setColumnWidth(i, 20*256);  //6 characters wide
        }
       // sheet.setColumnWidth(10, 10*256); //10 characters wide
		  try(FileOutputStream outputStream = new FileOutputStream(file)){
          workbook.write(outputStream);
          outputStream.close();
		  }catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void download(Button b) {
		FileDownloader fileDownloader = new FileDownloader(createResource());
	       fileDownloader.extend(b);
	}
	
	   private StreamResource createResource() {
		        return new StreamResource(()-> {
		            	byte[] array = null;
		            	ByteArrayOutputStream bos=null;
		                try {
		                	 array = Files.readAllBytes(Paths.get(path));
		                	 bos = new ByteArrayOutputStream();
		                      bos.write(array);
		                	  bos.close();
						} catch (FileNotFoundException e) {
		                } catch (IOException e) {}
		            
		                return new ByteArrayInputStream(bos.toByteArray());

		            }, name);
		        
		    }
	
	public String getName() {
		return name;
	}
	 private static Map<String, CellStyle> createStyles(Workbook wb){
	        Map<String, CellStyle> styles = new HashMap<>();
	        CellStyle style;
	        Font titleFont = wb.createFont();
	        titleFont.setFontHeightInPoints((short)18);
	        titleFont.setBold(true);
	        style = wb.createCellStyle();
	        style.setAlignment(HorizontalAlignment.CENTER);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFont(titleFont);
	        styles.put("title", style);

	        Font monthFont = wb.createFont();
	        monthFont.setFontHeightInPoints((short)11);
	        monthFont.setColor(IndexedColors.WHITE.getIndex());
	        style = wb.createCellStyle();
	        style.setAlignment(HorizontalAlignment.CENTER);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
	        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	        style.setFont(monthFont);
	        style.setWrapText(true);
	        styles.put("header", style);

	        style = wb.createCellStyle();
	        style.setAlignment(HorizontalAlignment.CENTER);
	        style.setWrapText(true);
	        style.setBorderRight(BorderStyle.THIN);
	        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
	        style.setBorderLeft(BorderStyle.THIN);
	        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
	        style.setBorderTop(BorderStyle.THIN);
	        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
	        style.setBorderBottom(BorderStyle.THIN);
	        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
	        styles.put("cell", style);

	        style = wb.createCellStyle();
	        style.setAlignment(HorizontalAlignment.CENTER);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
	        styles.put("formula", style);

	        style = wb.createCellStyle();
	        style.setAlignment(HorizontalAlignment.CENTER);
	        style.setVerticalAlignment(VerticalAlignment.CENTER);
	        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
	        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
	        styles.put("formula_2", style);

	        return styles;
	    }
}

 