package com.example.myapplication.view;

import java.util.List;

import com.example.myapplication.dao.CompanyDAO;
import com.example.myapplication.dao.Factory;
import com.example.myapplication.model.Company;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;




@SuppressWarnings("serial")
public class CompView extends VerticalLayout implements View{
	
	
    private Grid<Company> grid = new Grid<>(Company.class);
    private TextField filterText = new TextField();
	private final Factory f=Factory.getInstance();
	private final CompanyDAO service=f.getCompanyDAO();
	private final CompanyForm form= new CompanyForm(this);
	private FilterCompany fc= new FilterCompany();

	
	
    public CompView() {
    	
    setWidth(100,Unit.PERCENTAGE);
    	

    	filterText.setPlaceholder("filter by name...");
        filterText.addValueChangeListener(e -> updateList());
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        
        Button clearFilterTextBtn = new Button(FontAwesome.TIMES);
        clearFilterTextBtn.setDescription("Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterText.clear());
        
        CssLayout filtering = new CssLayout();
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        filtering.addComponents(filterText, clearFilterTextBtn);

        
        Button addCustomerBtn = new Button("Add new Company");
        addCustomerBtn.addClickListener(e -> {
           form.setCreated(true);
           grid.asSingleSelect().clear();
           form.setCompany(new Company());
           form.clearPanel();
           
        });
       
        Button downloadExcel = new   Button("Download to excel");

        HorizontalLayout toolbar = new HorizontalLayout(filtering, addCustomerBtn,downloadExcel);

        grid.setColumns("name", "inn", "address");
       
        HorizontalLayout main = new HorizontalLayout(grid, form);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);
        addComponents(toolbar, main);
    

        // fetch list of Customers from service and assign it to Grid
       updateList();

       form.setVisible(false);
       
       grid.asSingleSelect().addValueChangeListener(event -> {
           if (event.getValue() == null) {
              form.setVisible(false);
           } else {
              form.setCompany(event.getValue());
              form.employeesInCompany();
              form.updateCombo();
             
           }
       });
         form.setCreated(false);

    creatExcel(downloadExcel);
	}

	
	public void creatExcel(Button b) {
		   ExcelExport ee=new ExcelExport("src/main/resources/employees.xls");
		   
		   ee.creatSheet("companies");
		   ee.setMainTitle("Companyes");
		   ee.setColumnName("Название","Инн","Адрес");
		   
		   ee.createExcel(grid);
		   ee.download(b);
  
		
	}

	public void updateList() {
	  
       List<Company> customers =  fc.findAll(filterText.getValue(),service);
        grid.setItems(customers);
    
    }
}


