package com.example.myapplication.ui;

import java.util.Collection;
import java.util.List;

import com.vaadin.ui.Grid;

/**
 * 
 * Класс для упрощенной работы с Grid
 * 
 * @author user
 *
 * @param <T>
 */
@SuppressWarnings("serial")
public class MyGrid<T> extends Grid<T>{
	
   private int countColumn;
   private int length;
   private List<T> data;

    public MyGrid() {
		super();
	}
	
	public MyGrid(Class<T> class1) {
		super(class1);
	}
   
	public T getItem(int index) {
		return data.get(index);
	}
	
	
	public List<T> getDatas() {
		return data;
	}
	
	public int getIndex(T t) {
		return data.indexOf(t);
	}
	
	
	public boolean add(T t,Add add) {
		if(add.add()) {
			data.add(t);
			refresh();
			return true;
		}
		return false;
	}
	
	public boolean update(T t,Update update) {
		int i=getIndex(t);
		if(i==-1) return false;
		if(update.update()) {
			data.set(i, t);
			refresh();
			return true;
			}	
		return false;
	} 
	
/*	public String[] getRow(int i) {
		return getItem(i).toString().split(",");
	}*/
	
	public boolean remove(T t,Delete del) {
		if(del.delete()) {
		data.remove(t);	
		refresh();
		return true;
		}
	 return false;	
		
	}
	
	/**
	 * 
	 * 
	 * @param index
	 * @param del
	 * @return
	 */
	public boolean remove(int index,Delete del) {
		if(index>=data.size() || index<0) return false;;
			boolean res=del.delete();
		if(!res) return false;
		data.remove(index);
		refresh();
		return true;
	}

   @Override
	public void setItems(Collection<T> items) {
	   data=(List<T>) items;
	   length=data.size();
	   super.setItems(items);
		
	}

    @Override
	public void setColumns(String... columnIds) {
		super.setColumns(columnIds);
		countColumn=columnIds.length;
	}
   
   
   public int getCountColumn() {
    	return countColumn;
    }

   public void refresh() {
	   getDataProvider().refreshAll();
   }

   public int getSize() {
	   return length;
   }
/**
 * 
 * данные интерфейсы прездназначениы для изменения
 * данных в источнике, прежде чем они будут изменены в таблице.
 * 
 * 
 * @author amos57
 *
 */
   public interface Delete{
	   
	   boolean delete();
   }
	
   public interface Add{
	   
	   boolean add();
   }
   public interface Update{
	   
	   boolean update();
	   
   }	
}
